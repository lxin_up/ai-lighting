package com.kinglumi.ailighting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

// 不要用错包了 tk.mybatis.spring.annotation.MapperScan;
@MapperScan(basePackages = "com.kinglumi.ailighting.auth.mapper")
@SpringBootApplication
public class AiLightingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AiLightingApplication.class, args);
    }

}
