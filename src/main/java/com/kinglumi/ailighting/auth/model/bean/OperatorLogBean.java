package com.kinglumi.ailighting.auth.model.bean;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class OperatorLogBean implements Serializable {

    /**
     * 浏览器信息
     */
    private String browserInfo;
    /**
     * 请求URL
     */
    private String requestURL;
    /**
     *  请求类型(get、post、put、delete等)
     */
    private String httpMethod;
    /**
     * 客户端请求IP
     */
    private String requestIP;
    /**
     * 请求参数
     */
    private String requestParam;
    /**
     * 请求参数
     */
    private Object[] requestParams;
    /**
     * 操作的类和方法
     */
    private String operateClassMethod;
    /**
     * 耗时
     */
    private long consumeTime;
    /***
     * 响应结果
     */
    private String responseResult;
    /**
     * 操作描述
     */
    private String operateDesc;
    /**
     * 异常信息
     */
    private String exceptionMsg;
    /**
     * 操作时间
     */
    private Date operateTime;

}
