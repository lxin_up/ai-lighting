package com.kinglumi.ailighting.auth.model;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;


/**
 * TODO
 *  文件响应 model
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/9 13:44
 */

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@Slf4j
public class FileResponse extends BaseEntity implements Serializable {

    private String code;

    private String message;

    private String fileName;

    private String fileUrl;

    private boolean success;

    private boolean fail;

}
