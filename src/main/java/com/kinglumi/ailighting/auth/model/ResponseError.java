package com.kinglumi.ailighting.auth.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kinglumi.ailighting.auth.base.BaseEntity;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * TODO
 *  响应异常
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/9 13:49
 */

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@Slf4j
public class ResponseError extends BaseEntity implements Serializable {


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private List<String> errors;
}
