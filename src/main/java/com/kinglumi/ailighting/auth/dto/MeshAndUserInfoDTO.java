package com.kinglumi.ailighting.auth.dto;

import com.kinglumi.ailighting.auth.entity.UserInfo;
import lombok.Data;

import java.io.Serializable;

@Data
public class MeshAndUserInfoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    // 身为管理员 数目
    int adminNum = 0;
    // 参数数目
    int participantNum = 0;
    //总数
    int meshCountNum = 0;
    // user信息
    UserInfo userInfo;
}
