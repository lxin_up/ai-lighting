package com.kinglumi.ailighting.auth.util;

import com.kinglumi.ailighting.auth.constant.InterfaceConstants;
import com.kinglumi.ailighting.auth.vo.EchartsDataUtilVo;
import com.kinglumi.ailighting.auth.vo.EchartsDataVo;
import com.kinglumi.ailighting.auth.vo.ExhaustEmissionCountVo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 18:28 2022/5/15
 * @Modified By:
 */
public class EchartsDataUtil {

    public static EchartsDataUtilVo getDateFormat(String type, String startTime, String endTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(InterfaceConstants.DatePattern.DATE_TIME);
        LocalDateTime start = LocalDateTime.parse(startTime, formatter);
        LocalDateTime end = LocalDateTime.parse(endTime, formatter).plusSeconds(1);
        Duration duration = Duration.between(start, end);

        long between;
        String dateFormat;
        String postfix;

        switch (type) {
            case "1":
                between = start.toLocalDate().toEpochDay() - end.toLocalDate().toEpochDay();
                dateFormat = "%Y%m%d";
                postfix = "000000";
                break;
            case "2":
                end = end.minusSeconds(1);
                start = start.with(TemporalAdjusters.firstDayOfMonth());
                end = end.with(TemporalAdjusters.lastDayOfMonth());
                end = end.plusDays(1);
                duration = Duration.between(start, end);
                between = start.toLocalDate().until(end.toLocalDate(), ChronoUnit.MONTHS);
                dateFormat = "%Y%m";
                postfix = "01000000";
                break;
            default:
                between = duration.toHours();
                dateFormat = "%Y%m%d%H";
                postfix = "0000";
        }
        EchartsDataUtilVo eChartsDataUtilVo = new EchartsDataUtilVo();
        eChartsDataUtilVo.setBetween(between);
        eChartsDataUtilVo.setDateFormat(dateFormat);
        eChartsDataUtilVo.setPostfix(postfix);
        eChartsDataUtilVo.setFormatter(formatter);
        eChartsDataUtilVo.setStart(start);
        eChartsDataUtilVo.setEnd(end);
        eChartsDataUtilVo.setDuration(duration);
        return eChartsDataUtilVo;
    }

    public static EchartsDataVo buildCharts(List<ExhaustEmissionCountVo> resultList, Map<Date, Integer> map) {
        EchartsDataVo charts = new EchartsDataVo();
        resultList.forEach(t -> {
            charts.getXAxis().add(t.getTime());
            if (map.containsKey(t.getTime())) {
                charts.getYAxis().add(map.get(t.getTime()).toString());
            } else {
                charts.getYAxis().add("0");
            }
        });
        return charts;
    }

    public static EchartsDataVo buildCharts(Map<String, Integer> map) {
        EchartsDataVo charts = new EchartsDataVo();
        Set<String> key = map.keySet();
        for (String k : key) {
            charts.getXAxis().add(k);
            charts.getYAxis().add(map.get(k).toString());
        }
        return charts;
    }

    public static List<ExhaustEmissionCountVo> getMemberCountVOList(String type, EchartsDataUtilVo eChartsDataUtilVo) {
        List<ExhaustEmissionCountVo> resultList = new ArrayList<>();
        for (int i = 0; i < eChartsDataUtilVo.getBetween(); i++) {
            ExhaustEmissionCountVo count = new ExhaustEmissionCountVo();
            switch (type) {
                case "1":
                    count.setTime(eChartsDataUtilVo.getFormatter().format(eChartsDataUtilVo.getStart().plusDays(i)));
                    break;
                case "2":
                    count.setTime(eChartsDataUtilVo.getFormatter().format(eChartsDataUtilVo.getStart().plusMonths(i)));
                    break;
                default:
                    count.setTime(eChartsDataUtilVo.getFormatter().format(eChartsDataUtilVo.getStart().plusHours(i)));
                    break;
            }
            resultList.add(count);
        }
        return resultList;
    }


}