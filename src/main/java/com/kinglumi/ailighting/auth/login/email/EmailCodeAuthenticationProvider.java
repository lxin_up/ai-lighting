package com.kinglumi.ailighting.auth.login.email;


import com.kinglumi.ailighting.auth.constant.CommonConstant;
import com.kinglumi.ailighting.auth.constant.ServiceErrorEnum;
import com.kinglumi.ailighting.auth.exception.BaseBusinessException;
import com.kinglumi.ailighting.auth.redis.IRedisCache;
import com.kinglumi.ailighting.auth.service.auth.impl.EmailUserDetailsService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 短信登陆鉴权 Provider，要求实现 AuthenticationProvider 接口
 *
 * @author Lxin
 * @version 1.0
 * @date 2021/8/10 15:34
 */
public class EmailCodeAuthenticationProvider implements AuthenticationProvider {
    private EmailUserDetailsService userDetailsService;
    private IRedisCache redisCache;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        EmailCodeAuthenticationToken authenticationToken = (EmailCodeAuthenticationToken) authentication;
        //获取手机号码和验证码
        String mobile = determineMobile(authentication);
        String smsCode = (String) authenticationToken.getCredentials();
        checkSmsCode(mobile, smsCode);

        UserDetails userDetails = userDetailsService.loadUserByUsername(mobile);
        // 此时鉴权成功后，应当重新 new 一个拥有鉴权的 authenticationResult 返回
        return createSuccessAuthentication(userDetails, authentication, userDetails);
    }

    /**
     * 这个方法照抄AbstractUserDetailsAuthenticationProvider中的createSuccessAuthentication
     *
     * @param principal
     * @param authentication
     * @param user
     * @return
     */
    private EmailCodeAuthenticationToken createSuccessAuthentication(Object principal, Authentication authentication,
                                                                     UserDetails user) {
        // Ensure we return the original credentials the user supplied,
        // so subsequent attempts are successful even with encoded passwords.
        // Also ensure we return the original getDetails(), so that future
        // authentication events after cache expiry contain the details
        EmailCodeAuthenticationToken authenticationResult =
                new EmailCodeAuthenticationToken(principal, authentication.getCredentials(), user.getAuthorities());
        authenticationResult.setDetails(authentication.getDetails());
        return authenticationResult;
    }

    /**
     * 这行代码照抄了AbstractUserDetailsAuthenticationProvider中的determineUsername
     *
     * @param authentication
     * @return
     */
    private String determineMobile(Authentication authentication) {
        return (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
    }

    private void checkSmsCode(String mobile, String smsCode) {
        Object sessionCode = this.redisCache.get(CommonConstant.RedisKeys.EMAIL_CODE + mobile);
        if (sessionCode == null) {
            throw new BaseBusinessException(ServiceErrorEnum.NOT_CODE);
        }
        String code = ((String) sessionCode);
        if (!code.equals(smsCode)) {
            throw new BaseBusinessException(ServiceErrorEnum.CODE_ERROR);
        }
        /// 删除使用过的验证码
        this.redisCache.del(CommonConstant.RedisKeys.EMAIL_CODE + mobile);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        // 判断 authentication 是不是 SmsCodeAuthenticationToken 的子类
        return EmailCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public EmailUserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(EmailUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public IRedisCache getRedisCache() {
        return redisCache;
    }

    public void setRedisCache(IRedisCache redisCache) {
        this.redisCache = redisCache;
    }
}

