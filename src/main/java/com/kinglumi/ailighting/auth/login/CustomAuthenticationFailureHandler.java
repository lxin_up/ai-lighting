package com.kinglumi.ailighting.auth.login;

import com.alibaba.fastjson.JSON;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/10 15:34
 */
@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        ApiResult fail = ApiResult.fail(exception.getMessage());
        writer.write(JSON.toJSONString(fail));

    }
}
