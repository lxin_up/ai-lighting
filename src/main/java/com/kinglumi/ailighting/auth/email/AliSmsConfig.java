package com.kinglumi.ailighting.auth.email;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: carl
 * @Description:
 * @Date: Created in 8:44 2020/10/13
 * @Modified By:
 */
@Configuration
@ConfigurationProperties(prefix = "sms")
@Data
public class AliSmsConfig {

    private String aliAccesskeyId;

    private String aliAccesskeySecret;

    private String aliSign;

    private String aliDomain;
}
