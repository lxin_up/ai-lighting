package com.kinglumi.ailighting.auth.email;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/6 15:06
 */
@Component
@AllArgsConstructor
public class EmailService {

//    @Autowired
//    private MailSender javaMailSender;

//    @Value("${hwo.emailAddress}")
//    private String address;

    private final EmailSenderConfig senderConfig;

    /**
     * 功能描述: 发送邮件验证码
     *
     * @param emailAddress 发送目标
     * @author lxin
     * @date 2021/8/6 15:09
     */
    public Object sendEmail(String emailAddress) {
        JavaMailSenderImpl mailSender = senderConfig.getSender(0);

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailSender.getUsername());
        message.setTo(emailAddress);
        message.setSubject("Verify your Caimeta email address");
        String code = CodeUtil.generateVerifyCode(6);
        message.setText("    You have chosen this email address as your Caimeta account. To verify that you actually own this email address,\n" +
                "please enter the verification code below on the email verification page :\n                 "  + code + "           \n    The verification code will expire one hours after this email is sent.");
        mailSender .send(message);
        return code;
    }

    public Object sendEmailGuonei(String emailAddress) {
        JavaMailSenderImpl mailSender = senderConfig.getSender(0);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailSender.getUsername());
        message.setTo(emailAddress);
        message.setSubject("验证您的智谋纪电子邮件地址");
        String code = CodeUtil.generateVerifyCode(6);
        message.setText("    您已选择此电子邮件地址作为您的智谋纪帐户。要验证您是否确实拥有此电子邮件地址，请在电子邮件验证页面上输入下面的验证码 :\n                 "  + code + "           \n    验证码将在发送此电子邮件一小时后过期。");
        mailSender .send(message);
        return code;
    }

    /**
     * @Author: wangyinjie
     * @Description: 自定义短信
     * @Date: 19:11 2022/11/1
     * @param
     * @return:
     */
    public Object sendEmaiAndlApp(String emailAddress, Integer app) {
        JavaMailSenderImpl mailSender = senderConfig.getSender(app);
        String appName = "Caimeta";
        String sendEamilAddress = "caimeta@kinglumi.com";
        if(app == 1){
            appName = "Sensetrack";
            sendEamilAddress = mailSender.getUsername();
        }
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(sendEamilAddress);
        message.setTo(emailAddress);
        message.setSubject("Verify your "+appName+" email address");
        String code = CodeUtil.generateVerifyCode(6);
        message.setText("    You have chosen this email address as your "+appName+" account. To verify that you actually own this email address,\n" +
                "please enter the verification code below on the email verification page :\n                 "  + code + "           \n    The verification code will expire one hours after this email is sent.");
        mailSender .send(message);
        return code;
    }
}
