package com.kinglumi.ailighting.auth.vo;

import lombok.Data;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 18:27 2022/5/15
 * @Modified By:
 */
@Data
public class EchartsDataUtilVo {
    private long between;
    private String dateFormat;
    private String postfix;
    private DateTimeFormatter formatter;
    private LocalDateTime start;
    private LocalDateTime end;
    private Duration duration;
}
