package com.kinglumi.ailighting.auth.vo;

import lombok.Data;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 18:29 2022/5/15
 * @Modified By:
 */
@Data
public class ExhaustEmissionCountVo {

    private Integer total;

    private String time;

    private String channel;
}
