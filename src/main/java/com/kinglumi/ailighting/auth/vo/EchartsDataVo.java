package com.kinglumi.ailighting.auth.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 18:23 2022/5/15
 * @Modified By:
 */
@Data
@NoArgsConstructor
public class EchartsDataVo {

    private String title;

    private BigDecimal max;

    private BigDecimal min;

    private String unit;

    private List<String> legend = new ArrayList<>();

    private List<String> xAxis = new ArrayList<>();

    private List<Object> data = new ArrayList<>();

    private List<Object> ext = new ArrayList<>();

    private List<String> yAxis = new ArrayList<>();

    public EchartsDataVo trimCharts() {
        if (getData().isEmpty()) {
            this.setData(null);
        }
        if (getLegend().isEmpty()) {
            this.setLegend(null);
        }
        if (getXAxis().isEmpty()) {
            this.setXAxis(null);
        }
        if (getExt().isEmpty()) {
            this.setExt(null);
        }
        if (getYAxis().isEmpty()) {
            this.setYAxis(null);
        }
        return this;
    }
}