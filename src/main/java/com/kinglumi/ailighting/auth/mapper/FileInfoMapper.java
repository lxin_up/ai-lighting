package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.FileInfo;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/9 14:11
 */

public interface FileInfoMapper extends MyMapper<FileInfo> {

    public FileInfo latestVersion(String type);
    public FileInfo latestAppVersion(String type, Integer scope);
    public FileInfo latestVersionByBluetoothType(String type, Integer bluetoothType);
}
