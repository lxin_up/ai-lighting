package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.FeedBackInfo;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/10 16:57
 */
public interface FeedBackInfoMapper extends MyMapper<FeedBackInfo> {
}
