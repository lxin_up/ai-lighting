package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.RoleInfo;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lee
 */
public interface RoleInfoMapper extends MyMapper<RoleInfo> {

    List<RoleInfo> listRoleByUserId(String userId);

}
