package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.MeshInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author lee
 */
public interface MeshInfoMapper extends MyMapper<MeshInfo> {

    @Delete("delete from mesh_info where admin_id = #{userId}")
    void deleteByUserId(@Param("userId") Integer userId);

    @Delete("delete from mesh_info where id = #{userId}")
    void deleteByMeshId(@Param("userId") Integer userId);

    @Select("select * from mesh_info where admin_id = #{userId}")
    List<MeshInfo> selectByUserId(@Param("userId") Integer userId);

    List<MeshInfo> selectAllByUserId(@Param("userId") Integer userId);
}
