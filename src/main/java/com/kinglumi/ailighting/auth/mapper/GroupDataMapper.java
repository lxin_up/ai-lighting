package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.GroupData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GroupDataMapper extends MyMapper<GroupData> {

    List<GroupData> findDataByTime(@Param("meshId") String meshId, @Param("startTime") String startTime, @Param("endTime") String endTime);

}
