package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.UserInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * <p>
 * User Mapper 接口
 * </p>
 */
@Component
public interface UserMapper extends MyMapper<UserInfo> {


    public int getNewUserCount();

    @Delete("delete from user_info where id = #{userId}")
    void deleteByUserId(@Param("userId") Integer userId);

    public UserInfo getInfoByEmail(String email);
}
