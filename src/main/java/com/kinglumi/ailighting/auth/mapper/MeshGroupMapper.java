package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.MeshGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface MeshGroupMapper extends MyMapper<MeshGroup> {

    @Delete("delete from mesh_group where mesh_id = #{meshId}")
    void deleteByMeshId(@Param("meshId") Integer meshId);

    @Delete("delete from mesh_group where mesh_id = #{meshId} and group_id = #{groupId}")
    void deleteByMeshIdAndGroupId(@Param("meshId") Integer meshId, @Param("groupId") Integer groupId);

    @Select("select * from mesh_group where mesh_id = #{meshId} and group_id = #{groupId} limit 1")
    MeshGroup getByMeshIdAndGroupId(@Param("meshId") Integer meshId, @Param("groupId") Integer groupId);
}
