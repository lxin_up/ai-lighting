package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.TransferData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 19:36 2022/5/15
 * @Modified By:
 */
public interface TransferDataMapper extends MyMapper<TransferData> {

    List<TransferData> findDataByTime(@Param("meshId") String meshId, @Param("startTime") String startTime, @Param("endTime") String endTime);
}

