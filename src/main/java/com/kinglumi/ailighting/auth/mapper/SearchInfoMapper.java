package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.SearchInfo;
import org.apache.ibatis.annotations.Param;

/**
 * @author lee
 */
public interface SearchInfoMapper extends MyMapper<SearchInfo> {


    SearchInfo getSearchInfoByContent(@Param("searchContent") String searchContent,@Param("userId") Integer userId);

    void updateStatusToDelete(@Param("userId") int userId,@Param("activeStatus") int activeStatus);
}
