package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.bo.PermissionInfoBO;
import com.kinglumi.ailighting.auth.entity.PermissionInfo;

import java.util.List;

/**
 * @author lee
 */
public interface PermissionMapper extends MyMapper<PermissionInfo> {

    List<PermissionInfoBO> listPermissionInfoBO();

}
