package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.MeshUserLink;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author lee
 */
public interface MeshUserLinkMapper extends MyMapper<MeshUserLink> {

    @Delete("delete from mesh_user_link where user_id = #{userId}")
    void deleteByUserId(@Param("userId") Integer userId);

    @Delete("delete from mesh_user_link where mesh_id = #{meshId}")
    void deleteByMeshId(@Param("meshId") Integer meshId);

    @Select("select id,mesh_id,user_id,create_time from mesh_user_link  where mesh_id = #{meshId}")
    List<MeshUserLink> selectByMeshId(@Param("meshId") Integer meshId);

    @Delete("delete from mesh_user_link where user_id = #{userId} and  mesh_id = #{meshId}")
    void deleteByMeshIdAndUserId(Integer meshId, Integer userId);
}
