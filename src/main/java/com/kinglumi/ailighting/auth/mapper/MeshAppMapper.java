package com.kinglumi.ailighting.auth.mapper;

import com.kinglumi.ailighting.auth.base.MyMapper;
import com.kinglumi.ailighting.auth.entity.MeshApp;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 19:32 2022/5/18
 * @Modified By:
 */
public interface MeshAppMapper extends MyMapper<MeshApp> {
}
