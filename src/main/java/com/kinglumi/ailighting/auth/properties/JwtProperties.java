package com.kinglumi.ailighting.auth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:01
 */
@Data
@Component
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {

    /**
     * 密匙
     */
    private String apiSecretKey = "JWT_SECRET_KEY";

    /**
     * 过期时间-默认半个小时1800L
     * 7L 7天
     */
    private Long expirationTime = 7L;

    /**
     * 默认存放token的请求头
     */
    private String requestHeader = "Authorization";

    /**
     * 默认token前缀
     */
    private String tokenPrefix = "AI_Lighting-";
}
