package com.kinglumi.ailighting.auth.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TODO
 *  操作注解，用于日志记录;
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:06
 */

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OperatorLog {

    //操作描述
    String value() default "";
    //true:表示持久化日志，false：表示不持久化
    boolean persistent() default true;
}
