package com.kinglumi.ailighting.auth.service.biz;

import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.entity.FileInfo;
import com.kinglumi.ailighting.auth.mapper.FileInfoMapper;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/9 14:14
 */
@Service
@Slf4j
public class FileInfoService extends BaseService<FileInfoMapper, FileInfo> {

    @Resource
    FileInfoMapper fileInfoMapper;

    public FileInfo latestVersion(String type){
        return fileInfoMapper.latestVersion(type);
    }

    public FileInfo latestVersion(String type, Integer scope){
        return fileInfoMapper.latestAppVersion(type, scope);
    }

    public FileInfo latestVersionByBluetoothType(String type, Integer scope){
        return fileInfoMapper.latestVersionByBluetoothType(type, scope);
    }
}
