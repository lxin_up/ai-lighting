package com.kinglumi.ailighting.auth.service.auth;

import javax.servlet.http.HttpServletRequest;

import com.kinglumi.ailighting.auth.entity.FileInfo;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author Neo
 */
public interface FileSystemStorageService {

	void init();
	String saveFile(MultipartFile file, HttpServletRequest request);
	Resource loadFile(int id);
	Resource loadNowApp(int scope);
	FileInfo nowApp(int scope);
	Map<String,Object> deleteDataAndFile(int id);

}
