package com.kinglumi.ailighting.auth.service.aspect;

import com.kinglumi.ailighting.auth.model.bean.OperatorLogBean;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:28
 */
public interface OperationLogService {
    /**
     * 保存操作日志
     * @param operatorLogBean
     */
    void saveOperationLog(OperatorLogBean operatorLogBean);

}
