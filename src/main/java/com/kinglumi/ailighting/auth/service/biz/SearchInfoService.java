package com.kinglumi.ailighting.auth.service.biz;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.base.Query;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.entity.SearchInfo;
import com.kinglumi.ailighting.auth.entity.UserDetail;
import com.kinglumi.ailighting.auth.mapper.SearchInfoMapper;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author lee
 * @date 2021/9/1419:05
 */
@Service
@Slf4j
public class SearchInfoService extends BaseService<SearchInfoMapper, SearchInfo> {

    @Resource
    SearchInfoMapper searchInfoMapper;

    @Override
    public void insertSelective(SearchInfo entity) {

        if (entity == null) {
            String msg = "searchInfo is null";
            throw new IllegalArgumentException(msg);
        }

        String searchContent = StringUtils.trimToEmpty(entity.getSearchContent());
        if (StringUtils.isBlank(searchContent)) {
            String msg = "searchContent is empty";
            throw new IllegalArgumentException(msg);
        }
        Integer loginUserId = getLoginUserId();
        if (loginUserId == null) {
            String msg = "loginUserId is null";
            throw new IllegalArgumentException(msg);
        }

        if (loginUserId<0) {
            String msg = "loginUserId is invalid";
            throw new IllegalArgumentException(msg);
        }
        entity.setUserId(loginUserId);
        SearchInfo searchInfo = searchInfoMapper.getSearchInfoByContent(searchContent,loginUserId);
        if (searchInfo == null) {
            searchInfo = entity;
            searchInfo.setCreateTime(new Date());
            searchInfo.setLastModifiedTime(new Date());
            searchInfoMapper.insertSelective(searchInfo);
        } else {
            searchInfo.setLastModifiedTime(new Date());
            searchInfo.setActiveStatus(1);
            searchInfoMapper.updateByPrimaryKeySelective(searchInfo);
        }
    }

    public void updateStatusToDelete(){
        Integer loginUserId = getLoginUserId();
        searchInfoMapper.updateStatusToDelete(loginUserId,0);
    }


    @Override
    public ApiResult selectPageByQuery(Query query) {
        Integer loginUserId = getLoginUserId();
        query.put("userId", loginUserId);
        query.put("activeStatus", 1);
        Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
        List<SearchInfo> list = this.searchInfoMapper.selectByExample(this.query2example(query, false));
        return ApiResult.okPage(list, result.getTotal());
    }

    /**
     * 获取登录用户Id
     *
     * @return 用户Id
     */
    private Integer getLoginUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetail principal = (UserDetail) authentication.getPrincipal();
        return principal.getUserInfo().getId();
    }
}
