package com.kinglumi.ailighting.auth.service.biz;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.kinglumi.ailighting.auth.bo.SmsRequest;
import com.kinglumi.ailighting.auth.constant.ServiceErrorEnum;
import com.kinglumi.ailighting.auth.email.AliSmsConfig;
import com.kinglumi.ailighting.auth.entity.UserInfo;
import com.kinglumi.ailighting.auth.exception.BaseBusinessException;
import com.kinglumi.ailighting.auth.redis.IRedisCache;
import com.kinglumi.ailighting.auth.util.SmsCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 阿里大鱼短信
 *
 * @Author: carl
 * @Description:
 * @Date: Created in 8:40 2020/10/13
 * @Modified By:
 */
@Service
public class AliSmsService extends SmsService {


    @Autowired
    private IRedisCache redisCache;
    @Autowired
    private UserInfoService userService;
    /**
     * 验证码短信模板
     */
    public final static String CODE_SMS_TEMP = "SMS_248590363";

    private SmsRequest sms;

    public AliSmsService(AliSmsConfig config) {
        this.aliSmsConfig = config;
        //初始化
        init();
    }

    private void init() {
        sms = new SmsRequest();
        sms.setAppId(aliSmsConfig.getAliAccesskeyId());
        sms.setToken(aliSmsConfig.getAliAccesskeySecret());
        sms.setAutograph(aliSmsConfig.getAliSign());
    }

    @Override
    public String sendSms(String mobile) {
        // 根据用户名验证用户
//        UserInfo info1 = new UserInfo();
//        info1.setTelephone(mobile);
//        UserInfo userInfo = userService.selectOne(info1);
//        if(userInfo == null){
//            throw new BaseBusinessException(ServiceErrorEnum.AUTH_EMAIL_INCORRECT);
//        }
        CommonResponse response = sendMessage(mobile, CODE_SMS_TEMP);
        return response.getData();
    }

    public CommonResponse sendMessage(String phone, String sign) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI5t6zcXJ6HgBb7F8s67zw", "XGE7veuqOiZ9o3bxixksk2cyYvAvhS");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", "智谋纪");
        request.putQueryParameter("TemplateCode", sign);
        JSONObject jsonObject = new JSONObject();
        String code = SmsCodeUtil.generateVerifyCode(6);
        // 五分钟过期
        this.redisCache.set("smsCode" + phone, code, 300);
        jsonObject.put("code", code);
        request.putQueryParameter("TemplateParam", jsonObject.toJSONString());
        try {
            CommonResponse response = client.getCommonResponse(request);
            return response;
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return new CommonResponse();
    }

}
