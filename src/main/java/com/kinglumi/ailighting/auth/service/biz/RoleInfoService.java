package com.kinglumi.ailighting.auth.service.biz;

import cn.hutool.core.util.StrUtil;
import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.entity.RoleInfo;
import com.kinglumi.ailighting.auth.mapper.RoleInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/9 11:19
 */
@Service
@Slf4j
public class RoleInfoService extends BaseService<RoleInfoMapper, RoleInfo> {

    public List<RoleInfo> listRoleByUserId(String userId) {
        if (StrUtil.isEmpty(userId)) {
            return null;
        }
        return this.mapper.listRoleByUserId(userId);
    }

}
