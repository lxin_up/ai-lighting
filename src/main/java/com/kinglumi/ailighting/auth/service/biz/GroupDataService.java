package com.kinglumi.ailighting.auth.service.biz;

import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.entity.GroupData;
import com.kinglumi.ailighting.auth.mapper.GroupDataMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingInt;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 18:20 2022/5/15
 * @Modified By:
 */
@Service
@Slf4j
public class GroupDataService extends BaseService<GroupDataMapper, GroupData> {

    @Autowired
    private GroupDataMapper groupDataMapper;

    /**
     * 饼状图
     *
     * @Author: xiaonan on 2020/1/3 上午8:26
     * @param: startTime 开始时间  endTime 结束时间  type 按时间维度查询(小时、天、月) (日、月、年)
     * @return:
     * @Description
     */
    public Map<Integer, Integer> bingByTime(String meshId, String startTime, String endTime, String type) {
        if("1".equals(type)){
            List<GroupData> groupDataList = this.groupDataMapper.findDataByTime(meshId, startTime, endTime);
            Map<Integer, Integer> map = groupDataList.stream().collect(Collectors.groupingBy(GroupData::getGroupId, summingInt(GroupData::getPassengerNum)));
            return map;
        }
        return null;
    }
}
