package com.kinglumi.ailighting.auth.service.biz;

import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.entity.GroupData;
import com.kinglumi.ailighting.auth.entity.TransferData;
import com.kinglumi.ailighting.auth.mapper.GroupDataMapper;
import com.kinglumi.ailighting.auth.mapper.TransferDataMapper;
import com.kinglumi.ailighting.auth.util.EchartsDataUtil;
import com.kinglumi.ailighting.auth.vo.EchartsDataUtilVo;
import com.kinglumi.ailighting.auth.vo.EchartsDataVo;
import com.kinglumi.ailighting.auth.vo.ExhaustEmissionCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingInt;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 19:36 2022/5/15
 * @Modified By:
 */
@Service
public class TransferDataService extends BaseService<TransferDataMapper, TransferData> {

    @Autowired
    private TransferDataMapper transferDataMapper;

    @Autowired
    private GroupDataMapper groupDataMapper;

    /**
     * 树状图
     *
     * @Author: xiaonan on 2020/1/3 上午8:26
     * @param: startTime 开始时间  endTime 结束时间  type 按时间维度查询(小时、天、月) (日、月、年)
     * @return:
     * @Description
     */
    public Map<Object, Integer> shuByTime(String meshId, String startTime, String endTime, String type) {
        if("1".equals(type)){
            List<GroupData> groupDataList = this.groupDataMapper.findDataByTime(meshId, startTime, endTime);
            Map<Object, Integer> map = groupDataList.stream().collect(Collectors.groupingBy(GroupData::getReportingTime, summingInt(GroupData::getPassengerNum)));
            return map;
        }else if("2".equals(type)){
            List<TransferData> transferDataList = this.transferDataMapper.findDataByTime(meshId, startTime, endTime);
            Map<Object, Integer> map = transferDataList.stream().collect(Collectors.groupingBy(TransferData::getMonth, summingInt(TransferData::getTotalPassengerNum)));
            return map;
        }else if("3".equals(type)){

        }
        return null;
    }
}
