package com.kinglumi.ailighting.auth.service.biz;


import com.kinglumi.ailighting.auth.email.AliSmsConfig;

/**
 * SMS短信
 *
 * @Author: carl
 * @Description:
 * @Date: Created in 8:38 2020/10/13
 * @Modified By:
 */
public abstract class SmsService {

    /**
     * 阿里大鱼配置信息
     */
    AliSmsConfig aliSmsConfig;

    /**
     * 发送短信
     *
     * @param
     * @Author: carl
     * @Description:
     * @Date: 8:38 2020/10/13
     * @return:
     */
    public abstract String sendSms(String mobile);
}
