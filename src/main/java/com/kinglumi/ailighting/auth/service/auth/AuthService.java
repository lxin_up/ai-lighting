package com.kinglumi.ailighting.auth.service.auth;


import com.kinglumi.ailighting.auth.bo.*;

public interface AuthService {

    /**
     * 功能描述: 登录
     *
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 13:56
     */
    ApiResult login(LoginInfo loginInfo);

    /**
     * 功能描述: 注销
     *
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 13:56
     */
    ApiResult logout();

    /**
     * 功能描述:  刷新token
     *
     * @param token 旧token
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 13:55
     */
    ApiResult refreshToken(String token);

    /**
     * 功能描述: 注册
     *
     * @param registerInfo 注册信息
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 13:56
     */
    ApiResult register(RegisterInfo registerInfo);

    /**
     * 功能描述: 获取邮箱验证码
     *
     * @param email 邮箱
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 14:12
     */
    String emailCode(String email);
    String emailCodeByScope(String email, Integer scope);
    String codeByScopeAndApp(String email, Integer scope, Integer app);

    /**
     * 功能描述: 注册判断邮箱是否已经注册
     *
     * @param email 邮箱
     * @return java.lang.Boolean
     * @author lxin
     * @date 2021/8/6 16:22
     */
    Boolean isEmailExist(String email);

    /**
     * 功能描述: 注册判断用户名是否已经注册
     *
     * @param username 用户名
     * @return java.lang.Boolean
     * @author lxin
     * @date 2021/8/6 16:22
     */
    Boolean isUsernameExist(String username);

    /**
     * 功能描述:  修改密码 （有token）
     *
     * @param registerInfo
     * @return com.kinglumi.ailighting.auth.bo.ApiResult   
     * @author lxin
     * @date 2021/8/12 13:46
     */
    ApiResult resetPassword(ResetPassword registerInfo);

    /**
     * 功能描述: 忘记密码 （无token）
     *
     * @param registerInfo
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/12 13:46
     */
    ApiResult forgetPassword(ResetPassword registerInfo);

    /**
     * 功能描述: 修改密码只需要旧密码就好
     *
     * @author lxin
     * @date 2021/8/6 13:54
     */
    ApiResult updatePassword(UpdatePassword updatePassword);
}
