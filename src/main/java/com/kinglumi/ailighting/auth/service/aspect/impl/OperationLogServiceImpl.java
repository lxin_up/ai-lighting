package com.kinglumi.ailighting.auth.service.aspect.impl;

import com.alibaba.fastjson.JSON;
import com.kinglumi.ailighting.auth.model.bean.OperatorLogBean;
import com.kinglumi.ailighting.auth.service.aspect.OperationLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:28
 */
@Primary //@Primary表示如果一个接口有多个实现类，优先使用@Primary标注的类
@Service
public class OperationLogServiceImpl implements OperationLogService {

    private static  final  Logger logger = LoggerFactory.getLogger(OperationLogServiceImpl.class);

    @Override
    public void saveOperationLog(OperatorLogBean operatorLogBean) {
        //持久化略
       logger.info("用户访问日志", JSON.toJSONString(operatorLogBean));
    }
}
