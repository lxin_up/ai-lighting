package com.kinglumi.ailighting.auth.service.biz;

import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.entity.FeedBackInfo;
import com.kinglumi.ailighting.auth.mapper.FeedBackInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/10 16:59
 */
@Service
@Slf4j
public class FeedBackInfoService extends BaseService<FeedBackInfoMapper, FeedBackInfo> {

    @Override
    public void insertSelective(FeedBackInfo entity) {
        entity.setStatus("0");
        super.insertSelective(entity);
    }
}
