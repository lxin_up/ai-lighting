package com.kinglumi.ailighting.auth.service.aspect.impl;

import com.kinglumi.ailighting.auth.model.bean.OperatorLogBean;
import com.kinglumi.ailighting.auth.handler.absimpl.AbstractOperatorLogHandler;
import com.kinglumi.ailighting.auth.service.aspect.OperationLogService;
import com.kinglumi.ailighting.auth.task.OperationLogThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:28
 */
@Component
public class DefaultOperatorLogHandler extends AbstractOperatorLogHandler<OperatorLogBean> {
    @Autowired
    private OperationLogService operationLogService;
    public DefaultOperatorLogHandler(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        super(threadPoolTaskExecutor);
    }
    @Override
    public void persistenceLog(OperatorLogBean webLogBean) throws Exception {
        this.threadPoolTaskExecutor.execute(new OperationLogThread(operationLogService,webLogBean));
    }
}
