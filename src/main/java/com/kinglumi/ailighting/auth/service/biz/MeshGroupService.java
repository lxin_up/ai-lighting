package com.kinglumi.ailighting.auth.service.biz;

import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.constant.ServiceErrorEnum;
import com.kinglumi.ailighting.auth.entity.MeshGroup;
import com.kinglumi.ailighting.auth.exception.BaseBusinessException;
import com.kinglumi.ailighting.auth.mapper.MeshGroupMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class MeshGroupService extends BaseService<MeshGroupMapper, MeshGroup> {


    @Override
    public void insertSelective(MeshGroup entity) {

        if (Objects.isNull(entity)) {
            throw new BaseBusinessException(ServiceErrorEnum.PARAMETER_IS_EMPTY);
        }
        Integer groupId = entity.getGroupId();
        Integer meshId = entity.getMeshId();
        if (Objects.isNull(meshId) || Objects.isNull(groupId)) {
            throw new BaseBusinessException(ServiceErrorEnum.PARAMETER_IS_EMPTY);
        }
        MeshGroup meshGroup = getByMeshIdAndGroupIdEx(meshId, groupId);
        if (Objects.isNull(meshGroup)) {
            super.insertSelective(entity);
        } else {
            throw new BaseBusinessException(ServiceErrorEnum.MESHID_AND_GROUPID_IS_EXIT);
        }


    }

    public void deleteByMeshIdAndGroupId(Integer meshId, Integer groupId) {
        if (Objects.isNull(meshId) || Objects.isNull(groupId)) {
            throw new BaseBusinessException(ServiceErrorEnum.PARAMETER_IS_EMPTY);
        }

        this.mapper.deleteByMeshIdAndGroupId(meshId, groupId);

    }

    public MeshGroup getByMeshIdAndGroupId(Integer meshId, Integer groupId) {
        if (Objects.isNull(meshId) || Objects.isNull(groupId)) {
            throw new BaseBusinessException(ServiceErrorEnum.PARAMETER_IS_EMPTY);
        }
        MeshGroup meshGroup = getByMeshIdAndGroupIdEx(meshId, groupId);
        if (Objects.isNull(meshGroup)) {
            throw new BaseBusinessException(ServiceErrorEnum.SERVICE_ERROR_GROUP);
        }
        return meshGroup;
    }

    private MeshGroup getByMeshIdAndGroupIdEx(Integer meshId, Integer groupId) {
        MeshGroup meshGroup = new MeshGroup();
        meshGroup.setMeshId(meshId);
        meshGroup.setGroupId(groupId);
        List<MeshGroup> meshGroups = this.mapper.select(meshGroup);

        if (CollectionUtils.isEmpty(meshGroups)) {
            return null;
        }
        return meshGroups.get(0);
    }

    public int updateByMeshIdAndGroupId(MeshGroup meshGroup) {
        if (Objects.isNull(meshGroup)) {
            throw new BaseBusinessException(ServiceErrorEnum.PARAMETER_IS_EMPTY);
        }
        if (Objects.isNull(meshGroup.getMeshId()) || Objects.isNull(meshGroup.getGroupId())) {
            throw new BaseBusinessException(ServiceErrorEnum.PARAMETER_IS_EMPTY);
        }

        MeshGroup byMeshIdAndGroupId = getByMeshIdAndGroupIdEx(meshGroup.getMeshId(), meshGroup.getGroupId());
        if (Objects.isNull(byMeshIdAndGroupId)) {
            throw new BaseBusinessException(ServiceErrorEnum.SERVICE_ERROR_GROUP);
        }   
        if (Objects.nonNull(meshGroup.getGroupData())) {
            byMeshIdAndGroupId.setGroupData(meshGroup.getGroupData());
        }

        if (Objects.nonNull(meshGroup.getGroupName())) {
            byMeshIdAndGroupId.setGroupName(meshGroup.getGroupName());
        }

        if (Objects.nonNull(meshGroup.getActiveStatus())) {
            byMeshIdAndGroupId.setActiveStatus(meshGroup.getActiveStatus());
        }

        return this.mapper.updateByPrimaryKey(byMeshIdAndGroupId);
    }
}
