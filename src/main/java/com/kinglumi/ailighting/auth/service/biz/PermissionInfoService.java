package com.kinglumi.ailighting.auth.service.biz;

import com.kinglumi.ailighting.auth.base.BaseService;
import com.kinglumi.ailighting.auth.bo.PermissionInfoBO;
import com.kinglumi.ailighting.auth.entity.PermissionInfo;
import com.kinglumi.ailighting.auth.mapper.PermissionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/9 11:18
 */
@Service
@Slf4j
public class PermissionInfoService extends BaseService<PermissionMapper, PermissionInfo> {

    public List<PermissionInfoBO> listPermissionInfoBO() {
        return this.mapper.listPermissionInfoBO();
    }

}
