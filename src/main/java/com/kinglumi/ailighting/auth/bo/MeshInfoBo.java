package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 22:16 2022/8/30
 * @Modified By:
 */
@Data
public class MeshInfoBo {

    private Integer meshId;

    private Integer userId;
}
