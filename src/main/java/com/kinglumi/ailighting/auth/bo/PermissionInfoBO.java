package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:08
 */
@Data
public class PermissionInfoBO {

    private String id;

    private String permissionName;

    private String permissionUri;

    private String permissionMethod;

    private String roleName;

    private String roleCode;
}

