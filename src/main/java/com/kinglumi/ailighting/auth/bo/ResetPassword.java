package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/12 13:43
 */
@Data
public class ResetPassword {

    private String telephone;

    private String email;

    @NotBlank(message = "新设置的密码为空")
    private String password;

    @NotBlank(message = "验证码为空")
    private String code;
}
