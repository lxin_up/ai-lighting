package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Lxin
 * @version 1.0
 * @date 2022/2/35 13:43
 */
@Data
public class UpdatePassword {

    private String email;

    private String username;

    @NotBlank(message = "老密码")
    private String oldPassword;

    @NotBlank(message = "新密码")
    private String newPassword;

}
