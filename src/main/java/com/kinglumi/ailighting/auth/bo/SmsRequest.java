package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

/**
 * @Author: carl
 * @Description:
 * @Date: Created in 9:53 2020/10/13
 * @Modified By:
 */
@Data
public class SmsRequest {

    /** 用户的账号唯一标识" */
    private String sid;

    /** 用户密钥" */
    private String token;

    /** 创建应用时系统分配的唯一标示" */
    private String appId;

    /** 模板ID" */
    private String templateId;

    /** 模板中的替换参数" */
    private String param;

    /** 接收的手机号,多个手机号码以英文逗号分隔" */
    private String mobile;

    /** 用户透传ID，随状态报告返回" */
    private String uid;

    /** 短信类型" */
    private String type;

    /** 短信模板名称" */
    private String templateName;

    /** 短信签名" */
    private String autograph;

    /** 短信内容 可支持输入参数，参数示例“{1}”、“{2}”" */
    private String content;

}
