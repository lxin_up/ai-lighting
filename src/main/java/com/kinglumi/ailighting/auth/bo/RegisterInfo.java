package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/6 13:49
 */
@Data
public class RegisterInfo {

    private String email;

    private String telephone;

    private String areacode;

    @NotBlank(message = "用户名昵称为空")
    private String nickname;

    @NotBlank(message = "登陆密码为空")
    private String password;

    @NotBlank(message = "验证码为空")
    private String code;

}
