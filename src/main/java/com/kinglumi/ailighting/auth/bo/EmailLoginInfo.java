package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/10 16:04
 */
@Data
public class EmailLoginInfo {
    @NotBlank(message = "email为空")
    private String email;

    @NotBlank(message = "验证码为空")
    private String eCode;
}
