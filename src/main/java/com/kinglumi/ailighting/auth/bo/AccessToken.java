package com.kinglumi.ailighting.auth.bo;

import com.kinglumi.ailighting.auth.entity.UserInfo;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:07
 */
@Data
@Builder
public class AccessToken {
    private String loginAccount;
    private String nickname;
    private String token;
    private Date expirationTime;
    private UserInfo userInfo;
}
