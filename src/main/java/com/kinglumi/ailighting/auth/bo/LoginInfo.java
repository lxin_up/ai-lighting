package com.kinglumi.ailighting.auth.bo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:08
 */
@Data
public class LoginInfo {
    @NotBlank(message = "登陆用户名为空")
    private String username;

    @NotBlank(message = "登陆密码为空")
    private String password;

}
