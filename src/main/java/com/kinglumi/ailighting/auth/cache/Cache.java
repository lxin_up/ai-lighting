package com.kinglumi.ailighting.auth.cache;

import com.kinglumi.ailighting.auth.constant.CacheName;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:09
 */
public interface Cache {
    <T> T get(CacheName cacheName, String key, Class<T> clazz);

    void put(CacheName cacheName, String key, Object value);

    void remove(CacheName cacheName, String key);
}
