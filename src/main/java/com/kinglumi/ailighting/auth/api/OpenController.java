package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.service.auth.AuthService;
import com.kinglumi.ailighting.auth.service.auth.FileSystemStorageService;
import com.kinglumi.ailighting.auth.service.biz.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:31
 */
@RestController
public class OpenController {

    @Autowired
    private AuthService authService;
    @Autowired
    private FileSystemStorageService fileSystemStorageService;
    @Autowired
    private SmsService smsService;

    /**
     * 功能描述: 获取验证码
     *
     * @param email 注册信息
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 13:54
     */
    @GetMapping("/open/email/code")
    public ApiResult getEmailCode(String email) {
        return ApiResult.ok(authService.emailCode(email));
    }

    @GetMapping("/open/email/codeByScope")
    public ApiResult getEmailCodeByScope(String email, Integer scope) {
        return ApiResult.ok(authService.emailCodeByScope(email, scope));
    }

    @GetMapping("/open/email/codeByScopeAndApp")
    public ApiResult getEmailCodeByScope(String email, Integer scope, Integer app) {
        return ApiResult.ok(authService.codeByScopeAndApp(email, scope, app));
    }

    /**
     * @Author: wangyinjie
     * @Description: 手机注册验证码发送
     * @Date: 20:43 2022/8/11
     * @param
     * @return:
     */
    @GetMapping("/open/sms/code")
    public ApiResult getSmsCode(@RequestParam String phone) {
        String s = smsService.sendSms(phone);
        return ApiResult.ok(s);
    }

    @GetMapping("/open/download/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable int id) {

        Resource resource = fileSystemStorageService.loadFile(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + resource.getFilename())
                .body(resource);
    }

    @GetMapping("/api/anon")
    public ApiResult test01() {
        return ApiResult.ok("匿名访问成功");
    }

    @GetMapping("/api/user")
    public ApiResult test02() {
        return ApiResult.ok("USER用户访问成功");
    }

    @GetMapping("/api/admin")
    public ApiResult test03() {
        return ApiResult.ok("管理员用户访问成功");
    }

    @GetMapping("/api/test")
    public ApiResult test04() {
        return ApiResult.ok("test 访问  未配置权限并测试一次提交到git");
    }
}
