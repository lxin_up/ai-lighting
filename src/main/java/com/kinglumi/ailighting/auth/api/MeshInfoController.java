package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.base.BaseController;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.bo.MeshInfoBo;
import com.kinglumi.ailighting.auth.entity.MeshInfo;
import com.kinglumi.ailighting.auth.service.biz.MeshInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author lee
 * @date 2021/9/1110:38
 */
@RestController
@Slf4j
@RequestMapping("/api/mesh")
public class MeshInfoController extends BaseController<MeshInfoService, MeshInfo> {

    @PostMapping("/link")
    public ApiResult addMeshUserLink(Integer meshId, Integer userId) {
        this.baseBiz.addMeshUserLink(meshId, userId);
        return ApiResult.ok();
    }

    @DeleteMapping("/link")
    public ApiResult deleteMeshUserLink(Integer meshId, Integer userId) {
        this.baseBiz.deleteMeshUserLink(meshId, userId);
        return ApiResult.ok();
    }

    @GetMapping("/adminId")
    public ApiResult getUserAllMesh(Integer adminId) {
        return ApiResult.ok(this.baseBiz.getUserAllMesh(adminId));
    }

    @GetMapping("/adminId/new")
    public ApiResult getUserAllMeshNew(Integer adminId) {
        return ApiResult.ok(this.baseBiz.getUserAllMeshNew(adminId));
    }

    @GetMapping("/meshId")
    public ApiResult getMeshAllUser(Integer meshId) {
        return ApiResult.ok(this.baseBiz.getMeshAllUser(meshId));
    }

    @GetMapping("/json")
    public ApiResult joinMesh(String invitationCode) {
        return ApiResult.ok(this.baseBiz.joinMesh(invitationCode));
    }

    @GetMapping("/invitationCode")
    public ApiResult generateInvitationCode(Integer meshId) {
        return ApiResult.ok(this.baseBiz.generateInvitationCode(meshId));
    }

    @GetMapping("/user/info")
    public ApiResult meshAndUserInfo(Integer userId) {
        return ApiResult.ok(this.baseBiz.meshAndUserInfo(userId));
    }

    /**
     * 解散mesh
     *
     * @param meshId
     * @return
     */
    @DeleteMapping("/dissolution/{meshId}")
    public ApiResult dissolution(@PathVariable Integer meshId) {
        this.baseBiz.dissolution(meshId);
        return ApiResult.ok();
    }

    /**
     * 将用户退出mesh
     *
     * @param meshId
     * @return
     */
    @DeleteMapping("/user/{meshId}/{userId}")
    public ApiResult dissolutionUser(@PathVariable Integer meshId,@PathVariable Integer userId) {
        this.baseBiz.dissolutionUser(meshId,userId);
        return ApiResult.ok();
    }

    /**
     * 用户退出mesh
     * @param meshId
     * @param userId
     * @return
     */
    @DeleteMapping("/signOut")
    public ApiResult signOutUser(Integer meshId, Integer userId) {
        this.baseBiz.signOutUser(meshId,userId);
        return ApiResult.ok();
    }

    /**
     * @Author: wangyinjie
     * @Description: mesh转移
     * @Date: 9:53 2022/7/1
     * @param
     * @return:
     */
    @GetMapping("/transfer/mesh")
    public ApiResult transferMesh(Integer meshId, Integer userId, Integer id){
        this.baseBiz.transferMesh(meshId, userId, id);
        return ApiResult.ok();
    }
}
