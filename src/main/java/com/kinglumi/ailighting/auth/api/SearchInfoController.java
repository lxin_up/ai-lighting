package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.base.BaseController;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.entity.SearchInfo;
import com.kinglumi.ailighting.auth.service.biz.SearchInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lee
 * @date 2021/9/1419:04
 */
@RestController
@Slf4j
@RequestMapping("/api/search")
public class SearchInfoController extends BaseController<SearchInfoService, SearchInfo> {

    @Autowired
    SearchInfoService searchInfoService;

    @RequestMapping(value = {"clearAll"}, method = {RequestMethod.DELETE})
    @ResponseBody
    public ApiResult clearAll() {
        searchInfoService.updateStatusToDelete();
        return ApiResult.ok();
    }
}
