package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.base.BaseController;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.entity.MeshApp;
import com.kinglumi.ailighting.auth.service.biz.MeshAppService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 19:33 2022/5/18
 * @Modified By:
 */
@RestController
@Slf4j
@RequestMapping("/api/meshApp")
public class MeshAppController extends BaseController<MeshAppService, MeshApp> {

    /**
     * 通过meshId，uuid获取address，不存在新增,返回
     * @param app
     * @return
     */
    @PostMapping("/message")
    public ApiResult message(@Valid @RequestBody MeshApp app) {
        MeshApp meshApp = this.baseBiz.selectByMeshIdAndUuid(app.getMeshId(), app.getUuid());
        if(null == meshApp){
            meshApp = this.baseBiz.insertMeshApp(app.getMeshId(), app.getUuid());
        }
        return ApiResult.ok(meshApp);
    }

    /**
     * 通过meshId删除所有得meshApp
     * @param meshId
     * @return
     */
    @DeleteMapping("/deleteByMeshId")
    public ApiResult message(Integer meshId) {
        this.baseBiz.deleteByMeshId(meshId);
        return ApiResult.ok();
    }
}
