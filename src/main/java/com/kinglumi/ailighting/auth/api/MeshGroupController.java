package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.base.BaseController;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.entity.MeshGroup;
import com.kinglumi.ailighting.auth.service.biz.MeshGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/group")
public class MeshGroupController extends BaseController<MeshGroupService, MeshGroup> {

    /**
     * 提供通过 meshId 和 groupId 的删改查
     */
    @GetMapping("/deleteByMeshIdAndGroupId")
    public ApiResult deleteByMeshIdAndGroupId(Integer meshId, Integer groupId) {
        this.baseBiz.deleteByMeshIdAndGroupId(meshId, groupId);
        return ApiResult.ok();
    }

    @GetMapping("/getByMeshIdAndGroupId")
    public ApiResult getByMeshIdAndGroupId(Integer meshId, Integer groupId) {
        MeshGroup meshGroupList = this.baseBiz.getByMeshIdAndGroupId(meshId, groupId);
        return ApiResult.ok(meshGroupList);
    }

    @PostMapping("/updateByMeshIdAndGroupId")
    public ApiResult updateByMeshIdAndGroupId(@RequestBody MeshGroup meshGroup) {
        int i = this.baseBiz.updateByMeshIdAndGroupId(meshGroup);
        return ApiResult.ok(i);
    }




}
