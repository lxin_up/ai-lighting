package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.service.biz.TransferDataService;
import com.kinglumi.ailighting.auth.vo.EchartsDataVo;
import com.kinglumi.ailighting.auth.service.biz.GroupDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 16:08 2022/5/8
 * @Modified By:
 */
@RestController
@Slf4j
@RequestMapping("/api/group/data")
public class GroupDataController {

    @Autowired
    private GroupDataService groupDateService;

    @Autowired
    private TransferDataService transferDataService;

    /**
     * 树状图
     *
     * @Author: xiaonan on 2020/1/3 上午8:26
     * @param: startTime 开始时间  endTime 结束时间  type 按时间维度查询(小时、天、月)
     * @return:
     * @Description
     */
    @GetMapping("/shuByTime")
    public ApiResult shuByTime(@RequestParam String meshId, @RequestParam String startTime, @RequestParam String endTime, @RequestParam String type) {
        Map<Object, Integer> result = this.transferDataService.shuByTime(meshId, startTime, endTime, type);
        return ApiResult.ok(result);
    }

    /**
     * 饼状图
     *
     * @Author: xiaonan on 2020/1/3 上午8:26
     * @param: startTime 开始时间  endTime 结束时间  type 按时间维度查询(小时、天、月)
     * @return:
     * @Description
     */
    @GetMapping("/bingByTime")
    public ApiResult bingByTime(@RequestParam String meshId, @RequestParam String startTime, @RequestParam String endTime, @RequestParam String type) {
        Map<Integer, Integer> result = this.groupDateService.bingByTime(meshId, startTime, endTime, type);
        return ApiResult.ok(result);
    }
}
