package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.base.BaseController;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.entity.FeedBackInfo;
import com.kinglumi.ailighting.auth.service.biz.FeedBackInfoService;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/10 17:00
 */
@RestController
@Slf4j
@RequestMapping("/api/feedBack")
public class FeedBackController extends BaseController<FeedBackInfoService, FeedBackInfo> {
    @Autowired
    FeedBackInfoService feedBackInfoService;

    @RequestMapping(
            value = {""},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    @Override
    public ApiResult add(@RequestBody FeedBackInfo feedBackInfo) {
        feedBackInfo.setCreateTime(new Date());
        feedBackInfoService.insertSelective(feedBackInfo);
        return ApiResult.ok();
    }

}
