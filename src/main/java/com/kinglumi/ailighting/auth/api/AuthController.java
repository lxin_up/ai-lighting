package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.bo.*;
import com.kinglumi.ailighting.auth.provider.JwtProvider;
import com.kinglumi.ailighting.auth.service.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:31
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthService authService;
    @Autowired
    private JwtProvider jwtProvider;

//    /**
//     * 登录方法
//     * <p>
//     * loginAccount：user
//     * password：123456
//     *
//     * @param loginInfo
//     * @return ApiResult
//     */
//    @PostMapping("/login")
//    public ApiResult login(@Valid @RequestBody LoginInfo loginInfo) {

//        return authService.login(loginInfo);

//    }

    @PostMapping("/logout")
    public ApiResult logout() {
        return authService.logout();
    }

    @PostMapping("/refresh")
    public ApiResult refreshToken(HttpServletRequest request) {
        return authService.refreshToken(jwtProvider.getToken(request));
    }

    /**
     * 功能描述: 注册
     *
     * @param registerInfo 注册信息
     * @return com.kinglumi.ailighting.auth.bo.ApiResult
     * @author lxin
     * @date 2021/8/6 13:54
     */
    @PostMapping("/register")
    public ApiResult register(@Valid @RequestBody RegisterInfo registerInfo) {
        return authService.register(registerInfo);
    }

    /**
     * 功能描述: 重置密码 （有token）
     *
     * @author lxin
     * @date 2021/8/6 13:54
     */
    @PostMapping("/reset/password")
    public ApiResult resetPassword(@Valid @RequestBody ResetPassword registerInfo) {
        return authService.resetPassword(registerInfo);
    }

    /**
     * 功能描述: 重置密码 （有token）
     *
     * @author lxin
     * @date 2021/8/6 13:54
     */
    @PostMapping("/forget/password")
    public ApiResult forgetPassword(@Valid @RequestBody ResetPassword registerInfo) {
        return authService.forgetPassword(registerInfo);
    }

    /**
     * 功能描述: 修改密码只需要旧密码就好
     *
     * @author lxin
     * @date 2021/8/6 13:54
     */
    @PostMapping("/update/password")
    public ApiResult updatePassword(@Valid @RequestBody UpdatePassword updatePassword) {
        return authService.updatePassword(updatePassword);
    }

}
