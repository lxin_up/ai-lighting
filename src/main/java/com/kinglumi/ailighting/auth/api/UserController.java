package com.kinglumi.ailighting.auth.api;

import com.kinglumi.ailighting.auth.base.BaseController;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import com.kinglumi.ailighting.auth.entity.UserInfo;
import com.kinglumi.ailighting.auth.service.biz.UserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lee
 * @date 2021/9/1214:11
 */
@RestController
@Slf4j
@RequestMapping("/api/user")
public class UserController extends BaseController<UserInfoService, UserInfo> {

    @Autowired
    UserInfoService userInfoService;


    @RequestMapping(
            value = {"/userInfo"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public ApiResult userInfo() {
        int countUser = this.baseBiz.selectListAll().size();
        int newUserCount = userInfoService.getNewUserCount();
        Map<String, Integer> resultMap = new HashMap<>();
        resultMap.put("total", countUser);
        resultMap.put("newUser", newUserCount);
        return ApiResult.ok(resultMap);
    }

    /**
     * 删除 用户
     *
     * @param userId
     * @return
     */
    @DeleteMapping("/by/{userId}")
    public ApiResult deleteUser(@PathVariable Integer userId) {
        this.baseBiz.deleteUser(userId, true);
        return ApiResult.ok();
    }

    /**
     * 删除 用户
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    public ApiResult deleteUserInfo(Integer id) {
        this.baseBiz.deleteUser(id, false);
        return ApiResult.ok();
    }
}
