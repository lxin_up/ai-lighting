package com.kinglumi.ailighting.auth.handler.absimpl;
import com.kinglumi.ailighting.auth.handler.OperatorLogHandler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:06
 */
public abstract class AbstractOperatorLogHandler<T> implements OperatorLogHandler<T> {
    protected ThreadPoolTaskExecutor threadPoolTaskExecutor;
    public AbstractOperatorLogHandler(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.threadPoolTaskExecutor=threadPoolTaskExecutor;
    }
    /**
     * 处理日志
     * @param t 日志对象
     * @param isPersistent 是否持久化
     * @throws Exception
     */
    @Override
    public void processLog(T t,boolean isPersistent) throws Exception {
        if (customerWantsPersistenceLog(isPersistent)) {
            persistenceLog(t);
        }
    }
    /**
     * 日志持久化抽象方法，由子类实现
     * @param t 持久化日志
     * @throws Exception
     */
    @Override
    public abstract void persistenceLog(T t) throws Exception;
    /**
     * 是否需要持久化日志
     * @return boolean true持久化日志，false不持久化日志
     */
    @Override
    public boolean customerWantsPersistenceLog(boolean isPersistent) {
        return isPersistent;
    }
}
