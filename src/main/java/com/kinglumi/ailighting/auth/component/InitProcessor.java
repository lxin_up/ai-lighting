package com.kinglumi.ailighting.auth.component;

import com.kinglumi.ailighting.auth.bo.PermissionInfoBO;
import com.kinglumi.ailighting.auth.cache.Cache;
import com.kinglumi.ailighting.auth.constant.CacheName;
import com.kinglumi.ailighting.auth.service.biz.PermissionInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:10
 */
@Component
public class InitProcessor {
    @Autowired
    private PermissionInfoService permissionService;
    @Autowired
    private Cache caffeineCache;

    @PostConstruct
    public void init() {
        List<PermissionInfoBO> permissionInfoList = permissionService.listPermissionInfoBO();
        permissionInfoList.forEach(permissionInfo -> {
            caffeineCache.put(CacheName.PERMISSION, permissionInfo.getPermissionUri() + ":" + permissionInfo.getPermissionMethod(), permissionInfo);
        });
    }
}
