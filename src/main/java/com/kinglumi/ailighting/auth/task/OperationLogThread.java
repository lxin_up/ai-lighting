package com.kinglumi.ailighting.auth.task;

import com.alibaba.fastjson.JSON;
import com.kinglumi.ailighting.auth.model.bean.OperatorLogBean;
import com.kinglumi.ailighting.auth.service.aspect.OperationLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:28
 */
public class OperationLogThread implements Runnable{
    private static Logger logger= LoggerFactory.getLogger(OperationLogThread.class);
    private volatile OperationLogService operationLogService;
    private volatile OperatorLogBean operatorLogBean;

    public OperationLogThread(OperationLogService operationLogService,
                              OperatorLogBean operatorLogBean) {
        this.operationLogService=operationLogService;
        this.operatorLogBean=operatorLogBean;
    }

    @Override
    public void run() {
        try {
            if (logger.isInfoEnabled()) {
                logger.info("thread name " + Thread.currentThread().getName() + " start save operateLog " + JSON.toJSONString(operatorLogBean));
            }
            this.operationLogService.saveOperationLog(operatorLogBean);
            if (logger.isInfoEnabled()) {
                logger.info("thread name " + Thread.currentThread().getName() + "save operateLog success ");
            }
        } catch (Exception e) {
            logger.error("thread name "+Thread.currentThread().getName()+" save operateLog error",e);
        }
    }
}
