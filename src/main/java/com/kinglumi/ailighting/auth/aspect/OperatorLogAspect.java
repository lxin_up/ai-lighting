package com.kinglumi.ailighting.auth.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.kinglumi.ailighting.auth.annotation.OperatorLog;
import com.kinglumi.ailighting.auth.model.bean.OperatorLogBean;
import com.kinglumi.ailighting.auth.handler.OperatorLogHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * TODO
 *  日志切面
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/14 16:12
 */
@Aspect
@Order(10)
@Component
public class OperatorLogAspect {


    private static final Logger logger= LoggerFactory.getLogger(OperatorLogAspect.class);
    private ThreadLocal<Long> startTimeThreadLocal=new ThreadLocal<>();
    private ThreadLocal<OperatorLogBean> operatorLogBeanThreadLocal=new ThreadLocal<>();
    private ThreadLocal<Boolean> isPersistentThreadLocal=new ThreadLocal<>();
    public static final String TYPE_NAME_SERVLET="org.springframework.security.web.servletapi.HttpServlet3RequestFactory$Servlet3SecurityContextHolderAwareRequestWrapper";
    public static final String UNDERTOW_SERVLET_TYPE_NAME="io.undertow.servlet.spec.HttpServletRequestImpl";
    public static final String APACHE_REQUEST_FACADE="org.apache.catalina.connector.RequestFacade";
    public static final String MOCK_HTTP_SERVLET_REQUEST="org.springframework.mock.web.MockHttpServletRequest";

    /**
     * 日志处理类
     */
    @Autowired
    private OperatorLogHandler operatorLogHandler;

    // 切面的范围为前面定义的注解
    @Pointcut("@annotation(com.kinglumi.ailighting.auth.annotation.OperatorLog)")
    public void start() {}


    @Before("start()")
    public void before(JoinPoint joinPoint) {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        OperatorLogBean operatorLogBean=new OperatorLogBean();
        operatorLogBean.setBrowserInfo(request.getHeader("User-Agent"));
        operatorLogBean.setRequestURL(request.getRequestURL().toString());
        operatorLogBean.setHttpMethod(request.getMethod());
        operatorLogBean.setRequestIP(request.getRemoteAddr());
        //获取方法输入参数
        Object[] args=joinPoint.getArgs();
        List<Object> paramList=new ArrayList<>();
        if (args != null && args.length > 0) {
            for (int i=0; i<args.length ;i++) {
                if (args[i] != null) {
                    String typeName = args[i].getClass().getTypeName();
                    if (logger.isInfoEnabled()) {
                        logger.info(" request Parameter TypeName is "+typeName);
                    }
                    if (TYPE_NAME_SERVLET.equals(typeName) || UNDERTOW_SERVLET_TYPE_NAME.equals(typeName) ||
                            APACHE_REQUEST_FACADE.equals(typeName) || MOCK_HTTP_SERVLET_REQUEST.equals(typeName)) {
                        continue;
                    }
                    paramList.add(args[i]);
                }
            }
            String jsonParam= JSON.toJSONString(paramList, SerializerFeature.WriteNullListAsEmpty);
            operatorLogBean.setRequestParam(jsonParam);
        }
        operatorLogBean.setOperateClassMethod(joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName());
        MethodSignature methodSignature=(MethodSignature)joinPoint.getSignature();
        Method method=methodSignature.getMethod();
        //获取注解中的value值
        OperatorLog webLog=method.getAnnotation(OperatorLog.class);
        operatorLogBean.setOperateDesc(webLog.value());
        if (logger.isInfoEnabled()) {
            logger.info("请求参数：["+operatorLogBean.toString()+"]");
        }
        startTimeThreadLocal.set(System.currentTimeMillis());
        isPersistentThreadLocal.set(webLog.persistent());
        //将日志对象存储到ThreadLocal对象下，方便对象传递
        operatorLogBeanThreadLocal.set(operatorLogBean);
    }

    @AfterReturning(pointcut ="start()",returning = "object")
    public void afterReturning(Object object) {
        if (operatorLogBeanThreadLocal.get()!= null) {
            OperatorLogBean operatorLogBean = operatorLogBeanThreadLocal.get();
            if (startTimeThreadLocal.get() != null) {
                long startTime = startTimeThreadLocal.get();
                operatorLogBean.setConsumeTime(System.currentTimeMillis() - startTime);
            }
            operatorLogBean.setResponseResult(JSONObject.toJSONString(object));
            operatorLogBean.setOperateTime(new Date());
            if (logger.isInfoEnabled()) {
                logger.info("请求处理结束,参数：["+JSON.toJSONString(operatorLogBean)+"]");
            }
            Boolean persistentFlag=null;
            if (isPersistentThreadLocal.get()!= null) {
                persistentFlag=isPersistentThreadLocal.get();
            }

            try {
                operatorLogHandler.processLog(operatorLogBean, persistentFlag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        startTimeThreadLocal.remove();
        operatorLogBeanThreadLocal.remove();
        isPersistentThreadLocal.remove();
    }

    @AfterThrowing(pointcut = "start()",throwing = "throwable")
    public void afterThrowing(Throwable throwable) {
        logger.error("业务处理发生异常",throwable);
        if (logger.isInfoEnabled()) {
            logger.info("业务处理发生异常"+throwable.getMessage());
        }
        if (operatorLogBeanThreadLocal.get() != null) {
            OperatorLogBean operatorLogBean = operatorLogBeanThreadLocal.get();
            if (startTimeThreadLocal.get()!=null) {
                long startTime = startTimeThreadLocal.get();
                operatorLogBean.setConsumeTime(System.currentTimeMillis() - startTime);
            }
            operatorLogBean.setExceptionMsg(throwable.getMessage());
            operatorLogBean.setOperateTime(new Date());
            if (logger.isInfoEnabled()) {
                logger.info("请求处理异常,参数：["+JSON.toJSONString(operatorLogBean)+"]");
            }
            boolean persistentFlag=false;
            if (isPersistentThreadLocal.get()!= null) {
                persistentFlag=isPersistentThreadLocal.get();
            }
            try {
                operatorLogHandler.processLog(operatorLogBean,persistentFlag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        startTimeThreadLocal.remove();
        operatorLogBeanThreadLocal.remove();
        isPersistentThreadLocal.remove();
    }
}
