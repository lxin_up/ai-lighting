package com.kinglumi.ailighting.auth.base;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/9 10:02
 */

import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 自己的 Mapper
 * 特别注意，该接口不能被扫描到，否则会出错
 *
 * @author Lxin
 * @version 1.0
 * @date 2020/7/15 9:40
 */
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T>, IdsMapper<T> {
}
