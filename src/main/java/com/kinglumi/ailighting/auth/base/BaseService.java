package com.kinglumi.ailighting.auth.base;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/9 10:02
 */

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.kinglumi.ailighting.auth.bo.ApiResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.ParameterizedType;
import java.util.*;

/**
 * @author Lxin
 * @version 1.0
 * @date 2020/8/30 12:04
 */
public class BaseService<M extends MyMapper<T>, T extends BaseEntity> {
    @Autowired
    protected M mapper;

    public void setMapper(M mapper) {
        this.mapper = mapper;
    }

    public T selectOne(T entity) {
        return (T) this.mapper.selectOne(entity);
    }

    public T selectById(Object id) {
        return (T) this.mapper.selectByPrimaryKey(id);
    }

    public List<T> selectByIds(String ids) {
        return this.mapper.selectByIds(ids);
    }

    public List<T> selectList(T entity) {
        return this.mapper.select(entity);
    }

    public List<T> selectListAll() {
        return this.mapper.selectAll();
    }

    public Long selectCount(T entity) {
        return new Long((long) this.mapper.selectCount(entity));
    }

    public void insert(T entity) {
        this.mapper.insert(entity);
    }

    public void insertSelective(T entity) {
        entity.setCreateTime(new Date());
        this.mapper.insertSelective(entity);
    }

    public int insertSelectiveId(T entity) {
        entity.setCreateTime(new Date());
        this.mapper.insertSelective(entity);
        return entity.getId().intValue();
    }

    public int insertList(List<T> list) {
        Iterator var2 = list.iterator();

        while (var2.hasNext()) {
            T entity = (T) var2.next();
        }
        return this.mapper.insertList(list);
    }

    public void delete(T entity) {
        this.mapper.delete(entity);
    }

    public void deleteById(Object id) {
        this.mapper.deleteByPrimaryKey(id);
    }


    public void deleteByIds(List<Object> ids) {
        ids.parallelStream().forEach((id) -> this.mapper.deleteByPrimaryKey(id));
    }

    public void updateById(T entity) {
        this.mapper.updateByPrimaryKey(entity);
    }

    public void updateSelectiveById(T entity) {
        this.mapper.updateByPrimaryKeySelective(entity);
    }

    public void updateByExampleSelective(T entity, Object example) {
        this.mapper.updateByExampleSelective(entity, example);
    }

    public void updateByQuery(T entity, Query query) {
        this.mapper.updateByExampleSelective(entity, this.query2example(query, true));
    }

    public List<T> selectByQuery(Query query) {
        return this.mapper.selectByExample(this.query2example(query, true));
    }

    public List<T> selectByExample(Object example) {
        return this.mapper.selectByExample(example);
    }

    public int selectCountByExample(Object example) {
        return this.mapper.selectCountByExample(example);
    }

    public ApiResult selectPageByQuery(Query query) {
        Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
        List<T> list = this.mapper.selectByExample(this.query2example(query, false));
        return ApiResult.okPage(list, result.getTotal());
    }

    public Example query2example(Query query, boolean equal) {
        Class<T> clazz = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        Example example = new Example(clazz);
        Example.Criteria criteria = example.createCriteria();
        if (query.entrySet().size() > 0) {
            Iterator var6 = query.entrySet().iterator();

            while (var6.hasNext()) {
                Map.Entry<String, Object> entry = (Map.Entry) var6.next();
                Object obj = entry.getValue();

                if (StringUtils.isNumeric(obj.toString())) {
                    criteria.andEqualTo(entry.getKey(), entry.getValue());
                } else if (obj.toString().split(",").length > 1) {
                    String[] values = entry.getValue().toString().split(",");
                    criteria.andIn(entry.getKey(), new ArrayList(Arrays.asList(values)));
                } else if (equal) {
                    criteria.andEqualTo(entry.getKey(), entry.getValue());
                } else if (obj.toString().contains("-time-")) {
                    // 时间查询
                    String s = obj.toString();
                    String a = s.substring(0, s.indexOf("-time-"));
                    String b = s.substring(a.length() + 6);
                    criteria.andBetween("createTime", a, b);
                } else {
                    criteria.andLike(entry.getKey(), "%" + entry.getValue().toString() + "%");
                }
            }
        }
//        example.setOrderByClause("create_time DESC");
        return example;
    }


}
