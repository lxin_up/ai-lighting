package com.kinglumi.ailighting.auth.base;

import com.kinglumi.ailighting.auth.bo.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/9 11:28
 */
@Slf4j
public class BaseController<Biz extends BaseService, Entity extends BaseEntity> {

    @Autowired
    protected HttpServletRequest request;
    @Autowired
    protected Biz baseBiz;

    public BaseController() {
    }

    @RequestMapping(
            value = {""},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public ApiResult add(@RequestBody Entity entity) {
        this.baseBiz.insertSelective(entity);
        return ApiResult.ok();
    }

    @RequestMapping(
            value = {"/id"},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public ApiResult addId(@RequestBody Entity entity) {
        int id = this.baseBiz.insertSelectiveId(entity);
        return ApiResult.ok(id);
    }

    @RequestMapping(
            value = {"/addList"},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public ApiResult addList(@RequestBody List<Entity> list) {
        this.baseBiz.insertList(list);
        return ApiResult.ok();
    }

    @RequestMapping(
            value = {"/{id}"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public ApiResult get(@PathVariable int id) {
        Object o = this.baseBiz.selectById(id);
        return ApiResult.ok(o);
    }

    @RequestMapping(
            value = {"/ids"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public ApiResult get(@RequestParam String ids) {
        return ApiResult.ok(this.baseBiz.selectByIds(ids));
    }

    @RequestMapping(
            value = {"/list"},
            method = {RequestMethod.GET}
    )
    public ApiResult selectByQuery(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        List<Entity> list = this.baseBiz.selectByQuery(query);
        return ApiResult.ok(list);
    }

    @RequestMapping(
            value = {"/{id}"},
            method = {RequestMethod.PUT}
    )
    @ResponseBody
    public ApiResult update(@RequestBody Entity entity) {
        this.baseBiz.updateSelectiveById(entity);
        return ApiResult.ok();
    }

    @RequestMapping(
            value = {"/{id}"},
            method = {RequestMethod.DELETE}
    )
    @ResponseBody
    public ApiResult remove(@PathVariable int id) {
        this.baseBiz.deleteById(id);
        return ApiResult.ok();
    }

    @RequestMapping(
            value = {"/removeList"},
            method = {RequestMethod.POST}
    )
    @ResponseBody
    public ApiResult removeList(@RequestBody List<Integer> ids) {
        this.baseBiz.deleteByIds(ids);
        return ApiResult.ok();
    }

    @RequestMapping(
            value = {"/all"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public ApiResult all() {
        return ApiResult.ok(this.baseBiz.selectListAll());
    }

    @RequestMapping(
            value = {"/page"},
            method = {RequestMethod.GET}
    )
    @ResponseBody
    public ApiResult page(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        return this.baseBiz.selectPageByQuery(query);
    }

}

