package com.kinglumi.ailighting.auth.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/9 10:02
 */
@Data
public class BaseEntity {

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Column(name = "active_status")
    private Integer activeStatus;
}
