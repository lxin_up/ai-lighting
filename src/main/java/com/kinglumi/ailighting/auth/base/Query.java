package com.kinglumi.ailighting.auth.base;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Lxin
 * @version 1.0
 * @date 2020/8/30 12:07
 */
@Data
public class Query extends LinkedHashMap<String, Object> {
    private static final long serialVersionUID = 1L;
    /**
     * 当前页码
     */
    private int page = 1;

    /**
     * 每页条数
     */
    private int limit = 10;

    public Query(Map<String, Object> params){
        this.putAll(params);
        //分页参数
        if(params.get("page")!=null) {
            this.page = Integer.parseInt(params.get("page").toString());
        }
        if(params.get("limit")!=null) {
            this.limit = Integer.parseInt(params.get("limit").toString());
        }
        this.remove("page");
        this.remove("limit");
    }
}
