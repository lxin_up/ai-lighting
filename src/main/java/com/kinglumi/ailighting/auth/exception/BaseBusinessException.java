package com.kinglumi.ailighting.auth.exception;

import com.kinglumi.ailighting.auth.constant.ServiceErrorEnum;

public class BaseBusinessException extends RuntimeException {

    private Integer code;

    private String message;

    private String enMessage;

    public BaseBusinessException(ServiceErrorEnum errorEnum){
        this.code = errorEnum.getCode();
        this.message = errorEnum.getMessage();
        this.enMessage = errorEnum.getEnMessage();
    }

    public BaseBusinessException(Integer code, String message,String enMessage) {
//        super(message);
        this.code = code;
        this.message = message;
        this.enMessage = enMessage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEnMessage() {
        return enMessage;
    }

    public void setEnMessage(String enMessage) {
        this.enMessage = enMessage;
    }
}