package com.kinglumi.ailighting.auth.exception;

import com.kinglumi.ailighting.auth.bo.ApiResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理返回Json
 *
 * @author Lxin
 * @version 1.0
 * @date 2021/8/12 10:49
 */
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {
    /**
     * 所有异常报错
     *
     * @param request
     * @param exception
     * @return
     * @throws Exception
     */
    @ExceptionHandler(value = Exception.class)
    public ApiResult allExceptionHandler(HttpServletRequest request,
                                         Exception exception) throws Exception {
//        exception.printStackTrace();
        if(exception instanceof BaseBusinessException){
            BaseBusinessException baseBusinessException =  (BaseBusinessException) exception;
            return ApiResult.fail(baseBusinessException.getMessage(),baseBusinessException.getEnMessage(),baseBusinessException.getCode());
        }
        return ApiResult.failService(exception.getMessage());
    }

}
