package com.kinglumi.ailighting.auth.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Neo
 */
@Getter
@Setter
@AllArgsConstructor
public class FileNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;


}
