package com.kinglumi.ailighting.auth.constant;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 18:30 2022/5/15
 * @Modified By:
 */
public interface InterfaceConstants {

    interface DatePattern {
        String DEFAULT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
        String DEFAULT_DATE = "yyyy-MM-dd";
        String DEFAULT_TIME = "HH:mm:ss";
        String DATE_TIME = "yyyyMMddHHmmss";
        String DATE_TIME_MOUTH = "MMddHHmmss";
        String DATE_YYYYMMDD = "yyyyMMdd";
        String TIME_HOUR_MINUTE = "HHmm";
        String MOUTH_DAY = "MM/dd";
        String DEFAULT_DATE_MINUTE = "yyyy-MM-dd HH:mm";
    }
}

