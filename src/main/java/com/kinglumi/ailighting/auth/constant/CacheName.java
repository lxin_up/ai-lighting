package com.kinglumi.ailighting.auth.constant;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:02
 */
@AllArgsConstructor
public enum CacheName {

    USER("USER"),
    PERMISSION("PERMISSION");

    private final String cacheName;

    public static List<String> getCacheNames() {
        List<String> cacheNameList = new ArrayList<>(CacheName.values().length);
        CacheName[] values = CacheName.values();
        for (int i = 0; i < CacheName.values().length; i++) {
            cacheNameList.add(values[i].cacheName);
        }
        return cacheNameList;
    }

    public String getCacheName() {
        return cacheName;
    }
}

