package com.kinglumi.ailighting.auth.constant;

import lombok.AllArgsConstructor;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:03
 */
@AllArgsConstructor
public enum RoleEnum {

    USER("USER"),
    ADMIN("ADMIN");

    private final String value;

    public String getValue() {
        return this.value;
    }
}

