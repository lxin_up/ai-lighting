package com.kinglumi.ailighting.auth.constant;

public enum ServiceErrorEnum {
    // base
    NOT_DATA(ApiStatus.SERVICE_ERROR.getCode(), "对不起,查询不到数据", "Sorry, no data can be queried"),
    PARAMETER_IS_EMPTY(ApiStatus.SERVICE_ERROR.getCode(), "入参为空", "Input parameter is empty"),
    SIGN_OUT_FAIL(ApiStatus.SERVICE_ERROR.getCode(), "您是该mesh的管理员，不可以退出，请解散该mesh。", "You are the administrator of this mesh and cannot exit. Please dissolve this mesh."),
    NOT_ADMIN(ApiStatus.SERVICE_ERROR.getCode(), "此账户不是后台管理员不允许进行此操作", "This account is not a background administrator. This operation is not allowed"),
    // base

    //auth
    AUTH_VERIFICATION_EXPIRED(ApiStatus.SERVICE_ERROR.getCode(), "对不起，当前验证码已过期或者您的邮箱输入不正确", "Sorry, the current verification code has expired or your email is entered incorrectly"),
    AUTH_VERIFICATION_INCORRECT(ApiStatus.SERVICE_ERROR.getCode(), "对不起，验证码不正确", "Sorry, the verification code is incorrect"),
    AUTH_EMAIL_USED(ApiStatus.SERVICE_ERROR.getCode(), "当前邮箱已经被注册，请不要重复注册", "The current mailbox has been registered. Please do not register again"),
    AUTH_TELEPHONE_USED(ApiStatus.SERVICE_ERROR.getCode(), "当前手机号已经被注册，请不要重复注册", "The current mobile phone number has been registered. Please do not register again"),
    AUTH_NOT_END_MAIL(ApiStatus.SERVICE_ERROR.getCode(), "对不起，请不要重复发送邮件。请在10分钟后重试", "Sorry, please don't send mail again. Please try again in 10 minutes"),
    AUTH_EMAIL_INCORRECT(ApiStatus.SERVICE_ERROR.getCode(), "您输入的邮箱不存在", "The email you entered does not exist"),
    AUTH_TELEPHONE_INCORRECT(ApiStatus.SERVICE_ERROR.getCode(), "您输入的手机号不存在", "The mobile number you entered does not exist"),
    AUTH_UPDATE_INCORRECT(ApiStatus.SERVICE_ERROR.getCode(), "您输入的账户无法匹配旧密码", "The account you entered cannot match the old password"),
    USERNAME_INCORRECT(ApiStatus.SERVICE_ERROR.getCode(), "你输入的账户不正确，无法找到您的账户", "The account you entered is incorrect. Your account cannot be found"),
    NOTFUND_USER(ApiStatus.SERVICE_ERROR.getCode(), "转移得用户邮箱不存在该用户", "The mailbox of the transferred user does not exist"),
    NOTFUND_MESH(ApiStatus.SERVICE_ERROR.getCode(), "mesh不存在或已销毁", "Mesh does not exist or has been destroyed"),
    //auth

    // mesh
    JOIN_MESH_FORMAT(ApiStatus.SERVICE_ERROR.getCode(), "对不起，当前邀请码格式不正确", "Sorry, the current invitation code is not in the correct format."),
    JOIN_MESH_EXPIRED(ApiStatus.SERVICE_ERROR.getCode(), "对不起，当前邀请码已过期，或者您的meshID不正确", "Sorry, the current invitation code has expired, or your meshid is incorrect"),
    JOIN_MESH_INCORRECT(ApiStatus.SERVICE_ERROR.getCode(), "对不起，邀请码不正确", "Sorry, the invitation code is incorrect."),
    JOIN_MESH_ALREADY(ApiStatus.SERVICE_ERROR.getCode(), "对不起,你已经是mesh的成员了", "Sorry, you're already a member of mesh."),
    JOIN_MESH_INVITATION_NOT_EXPIRED(ApiStatus.SERVICE_ERROR.getCode(), "对不起，当前邀请码还未过期", "Sorry, the current invitation code has not expired"),
    MESH_ADMINISTRATOR(ApiStatus.SERVICE_ERROR.getCode(), "对不起,您已经是mesh的管理员,无法进行当前操作", "Sorry, you are already an administrator of mesh, unable to perform the current operation"),
    MESH_NOT_ADMINISTRATOR(ApiStatus.SERVICE_ERROR.getCode(), "对不起,您不是当前mesh的管理员,无法进行当前操作", "Sorry, you are not the administrator of the current mesh and cannot perform the current operation."),
    MESH_NOT_MEMBER(ApiStatus.SERVICE_ERROR.getCode(), "对不起,当前用户不是mesh的成员", "Sorry, the current user is not a member of mesh"),
    // mesh


    MESHID_AND_GROUPID_IS_EXIT(ApiStatus.SERVICE_ERROR.getCode(), "当前 mesh和group 已经存在关联", "Currently, mesh and group are already associated"),
    GROUP_NOT_DATA(ApiStatus.SERVICE_ERROR.getCode(), "对不起,查询不到数据", "Sorry, no data can be queried"),
    SERVICE_ERROR_GROUP(ApiStatus.SERVICE_ERROR_GROUP.getCode(), "对不起,查询不到数据", "Sorry, no data can be queried"),

    //token
    TOKEN_MISS(ApiStatus.UNAUTHORIZED.getCode(), "账号已被他人登录，访问此资源请重新登录", "The account has been logged in by others. Please log in again to access this resource"),
    PASSWORD_ERROR(ApiStatus.FAIL.getCode(), "登录失败，密码不正确", "Login failed, incorrect password."),
    NOT_USER(ApiStatus.FAIL.getCode(), "用户名不存在，登录失败", "User name does not exist, login failed."),
    NOT_CODE(ApiStatus.FAIL.getCode(), "未检测到应用程序验证码", "Application verification code not detected."),
    CODE_ERROR(ApiStatus.FAIL.getCode(), "验证码错误", "Verification code error."),

    //file
    NOT_CREATE_UPLOAD(ApiStatus.FAIL.getCode(), "验证码错误", "Could not create upload dir!"),
    NOT_UPLOAD_FILE(ApiStatus.FAIL.getCode(), "验证码错误", "Could not upload file."),
    NOT_FIND_FILE(ApiStatus.FAIL.getCode(), "验证码错误", "Could not find file."),
    NOT_DOWNLOAD_FILE(ApiStatus.FAIL.getCode(), "验证码错误", "Could not download file."),
    NOT_ID(ApiStatus.FAIL.getCode(), "id", "id not find.")
    ;

    private Integer code;
    private String message;
    private String enMessage;

    ServiceErrorEnum(Integer code, String message, String enMessage) {
        this.code = code;
        this.message = message;
        this.enMessage = enMessage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEnMessage() {
        return enMessage;
    }

    public void setEnMessage(String enMessage) {
        this.enMessage = enMessage;
    }
}
