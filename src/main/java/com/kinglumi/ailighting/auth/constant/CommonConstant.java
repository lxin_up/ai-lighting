package com.kinglumi.ailighting.auth.constant;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/6 14:53
 */
public interface CommonConstant {

    /**
     * 功能描述: 项目中 redis key
     *
     * @author lxin
     * @date 2021/8/6 14:55
     */
    interface RedisKeys {
        // 邮件验证码发送次数 限制
        String EMAIL_SEND_TIME = "emailCode:sendTime:";
        // 邮件验证码
        String EMAIL_CODE = "emailCode:code:";
        // mesh 加入 邀请码
        String INVITATION_CODE= "invitationCode:";
    }

    /**
     * 功能描述: 项目中 的静态字符串
     *
     * @author lxin
     * @date 2021/8/6 14:55
     */
    interface Common {
        // 常规登陆 账户名 密码
        String LOGIN_TYPE_USER = "1";
    }
}
