package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 19:27 2022/5/18
 * @Modified By:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeshApp extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "mesh_id")
    private Integer meshId;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "address")
    private Integer address;
}
