package com.kinglumi.ailighting.auth.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kinglumi.ailighting.auth.base.BaseEntity;
import java.util.Date;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author lee
 * @date 2021/9/1419:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchInfo extends BaseEntity implements Serializable {

    @Column(name = "search_content")
    private String searchContent;

    @Column(name = "last_modified_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifiedTime;

    @Column(name = "user_id")
    private Integer userId;
}
