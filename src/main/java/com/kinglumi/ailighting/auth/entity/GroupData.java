package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class GroupData extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Mesh ID
     */
    private Integer meshId;

    /**
     * 上报时间
     */
    private Long reportingTime;

    /**
     * 总客流人数
     */
    private Integer passengerNum;

    /**
     * 分组id
     */
    private Integer groupId;
}
