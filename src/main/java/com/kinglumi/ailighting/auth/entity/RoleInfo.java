package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class RoleInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String roleName;

    private String roleCode;

    private String roleRemark;

}
