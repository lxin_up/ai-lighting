package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * TODO
 *
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/9 14:06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo extends BaseEntity implements Serializable {

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件地址
     */
    private String fileUrl;

    /**
     * 文件类型
     */
    private String type;

    /**
     * 固件类型
     */
    private Integer bluetoothType;

    /**
     * 文件版本号
     */
    private String fileVersion;

    /**
     * 更新内容
     */
    private String updateMessage;

    /**
     * 是否强制更新
     */
    private Boolean isForceUpdate;

    /**
     * 更新内容
     */
    private String englishMessage;

    /**
     * 当前版本
     */
    private Boolean nowApp;

    /**
     * 作用范围 1国内 2海外
     */
    private Integer scope;

    /**
     * 为新型固件新增的json字段
     */
    private String jsonData;

}
