package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Lxin
 * @version 1.0
 * @date 2021/8/4 15:05
 */
@Data
@Table(name = "player")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PermissionInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String permissionName;

    private String permissionUri;

    private String permissionMethod;

}

