package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * TODO
 *  反馈信息
 * @author Neo
 * @version 1.0.0
 * @date 2021/9/10 16:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class FeedBackInfo extends BaseEntity implements Serializable {

    private String email;

    private String content;

    private String status;

    private String introduction;

}
