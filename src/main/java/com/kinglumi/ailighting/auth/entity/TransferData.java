package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: wangyinjie
 * @Description:
 * @Date: Created in 19:35 2022/5/15
 * @Modified By:
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TransferData extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Mesh ID
     */
    private Integer meshId;

    /**
     * 上报时间
     */
    private Long reportingTime;

    /**
     * 总客流人数
     */
    private Integer totalPassengerNum;

    /**
     * 分组数量
     */
    private Integer numberGroups;

    /**
     * 年
     */
    private String year;

    /**
     * 月
     */
    private String month;

}