package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * @author lee
 * @date 2021/9/1111:01
 */
@Data
@NoArgsConstructor
public class MeshUserLink extends BaseEntity implements Serializable {

    @Column(name = "mesh_id")
    private Integer meshId;

    @Column(name = "user_id")
    private Integer userId;

    public MeshUserLink(Integer meshId, Integer userId) {
        this.meshId = meshId;
        this.userId = userId;
        super.setActiveStatus(1);
        super.setCreateTime(new Date());
    }

    public MeshUserLink(Integer meshId) {
        this.meshId = meshId;
    }
}
