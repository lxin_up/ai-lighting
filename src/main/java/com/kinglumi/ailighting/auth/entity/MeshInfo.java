package com.kinglumi.ailighting.auth.entity;

import com.kinglumi.ailighting.auth.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * @author lee
 * @date 2021/9/1110:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeshInfo extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "mesh_name")
    private String meshName;

    @Column(name = "admin_id")
    private Integer adminId;

    @Column(name = "mesh_configuration")
    private String meshConfiguration;

    @Column(name = "username")
    private String userName;

    @Column(name = "nickname")
    private String nickname;

    /**
     * 成员数目
     */
    @Transient
    private Integer memberNum = 0;

    /**
     * 分组数目
     */
    @Transient
    private Integer groupNum = 0;

    @Transient
    private List<UserInfo> userInfoList;

    public MeshInfo(Integer adminId) {
        this.adminId = adminId;
    }
}
